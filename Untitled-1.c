#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <dirent.h>
#include <stdbool.h>

#define MAX_PATH 1024
#define MAX_COMMAND 2048
#define MAX_FILENAME 256
#define MAX_URL 256
#define MAX_GAMBAR 15
#define DELAY_BETWEEN_GAMBAR 5
#define DELAY_BETWEEN_FOLDER 30

volatile sig_atomic_t done = 0;

void handle_signal(int signum) {
    done = 1;
}

void create_folder(char* foldername) {
    mkdir(foldername, 0777);
}

void download_gambar(char* foldername, time_t t) {
    char url[MAX_URL];
    char filename[MAX_FILENAME];
    int i;

    for (i = 0; i < MAX_GAMBAR; i++) {
        sprintf(url, "https://picsum.photos/%d", (int)(t % 1000) + 50);
        sprintf(filename, "%s/%ld_%d.jpg", foldername, t, i);

        pid_t pid = fork();
        if (pid == 0) {
            execlp("wget", "wget", url, "-O", filename, NULL);
            exit(0);
        }

        sleep(DELAY_BETWEEN_GAMBAR);
    }
}

void zip_folder(char* foldername) {
    char command[MAX_COMMAND];
    sprintf(command, "zip -r %s.zip %s", foldername, foldername);
    system(command);
}

void delete_folder(char* foldername) {
    char command[MAX_COMMAND];
    sprintf(command, "rm -rf %s", foldername);
    system(command);
}

void generate_killer(char* filename) {
    FILE* fp = fopen(filename, "w");
    fprintf(fp, "#include <stdio.h>\n#include <stdlib.h>\n#include <unistd.h>\n\nint main() {\n    execlp(\"killall\", \"killall\", \"-9\", \"%s\", NULL);\n    return 0;\n}", strrchr(filename, '/') + 1);
    fclose(fp);
    chmod(filename, 0777);
}

void generate_killer_and_kill(char* filename) {
    generate_killer(filename);
    execl(filename, filename, NULL);
}

void mode_a() {
    signal(SIGINT, handle_signal);

    char foldername[MAX_FILENAME];
    time_t t;

    while (!done) {
        t = time(NULL);
        strftime(foldername, MAX_FILENAME, "%Y-%m-%d_%H:%M:%S", localtime(&t));
        create_folder(foldername);
        download_gambar(foldername, t);

        while (!done && (time(NULL) - t) < DELAY_BETWEEN_FOLDER) {
            sleep(1);
        }

        if (!done) {
            zip_folder(foldername);
            delete_folder(foldername);
        }
    }

    generate_killer_and_kill("./killer");
}

void mode_b() {
    signal(SIGINT, handle_signal);

    DIR* dir;
    struct dirent* entry;
    char foldername[MAX_FILENAME];
    time_t t;
    bool all_zipped;

    while (!done) {
        all_zipped = true;
        dir = opendir(".");
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR && strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
            sprintf(foldername, "%s", entry->d_name);
            if (foldername[strlen(foldername)-4] != '.' || strcmp(&foldername[strlen(foldername)-4], ".zip") != 0) {
                all_zipped = false;
                t = time(NULL);
                zip_folder(foldername);
                delete_folder(foldername);
                sleep(DELAY_BETWEEN_FOLDER);
            }
        }
    }
    closedir(dir);

    if (all_zipped) {
        break;
    }
    }
generate_killer_and_kill("./killer");
}



int main(int argc, char* argv[]) {
if (argc != 2 || (strcmp(argv[1], "a") != 0 && strcmp(argv[1], "b") != 0)) {
printf("Usage: %s [a|b]\n", argv[0]);
return 0;
}

if (strcmp(argv[1], "a") == 0) {
    mode_a();
} else if (strcmp(argv[1], "b") == 0) {
    mode_b();
}

return 0;
}
